var reporter = require('cucumber-html-reporter');
var http = require("https");

// Создание отчета cucumber в формате html
//    - project - имя проекта - tardis, tardis_agent
//
CreateReport("tardis_agent");

function CreateReport(project){

    if (project == "tardis_agent"){
        var requestOptions = {
            "method": "GET",
            "hostname": "beta-bp.42clouds.com",
            "port": null,
            "path": "/bp_base5-int/hs/api_v2/AutoTest/1330",
            "headers": {
              "content-length": "0",
              "authorization": "Basic VGFyZGlzOm1uZV8xNjdIRVY="
            }
          };
        var brand = "Tardis agent";   
    } else if (project == "tardis"){
        var requestOptions = {
            "method": "GET",
            "hostname": "beta-tardis.42clouds.com",
            "port": null,
            "path": "/tardis/hs/api_v1/test",
            "headers": {
              "content-length": "0"
            }
          };
          var brand = "Tardis";
    }
 
    var req = http.request(requestOptions, function (res) {
        
        var chunks = [];
    
        res.on("data", function (chunk) {
            chunks.push(chunk);
        });
        
        res.on("end", function () {
            
            var version = "";

            try {
                let response = Buffer.concat(chunks);
                if (project == "tardis_agent") {
                    var strRelease = response.toString().match(/tardisagent \d.\d.\d.\d/gmi)[0];
                    var version = strRelease.replace("TardisAgent ", "");
                } else if (project == "tardis") {
                    var strRelease = response.toString().match(/Release: \d.\d.\d.\d/gmi)[0];
                    var version = strRelease.replace("Release: ", "");
                }    
            }catch(e){  console.log(e);  }

            var fullBrandName = `${brand} ${version} - ${CurrentDateFormat()}`;

            var options = {
                theme: 'bootstrap',
                jsonFile: 'CucumberJson.json',
                output: 'cucumber_report.html',
                brandTitle: fullBrandName,
                reportSuiteAsScenarios: true,
                scenarioTimestamp: true,
                launchReport: true,
                metadata: {
                    "Test Environment": "STAGING",
                    "Browser": "Chrome  54.0.2840.98",
                    "Platform": "Windows 10",
                    "Parallel": "Scenarios",
                    "Executed": "Remote",
                    "Date": CurrentDateFormat()
                }
            };
         
            reporter.generate(options);
        });
    });
      
    req.end();

}

function CurrentDateFormat() {
    var now = new Date();
    let day = now.getDate();
    let month = now.getMonth() + 1;
    let year = now.getFullYear();
    let hours = now.getHours();
    let minutes = now.getMinutes();
    let seconds = now.getSeconds();
    if(day < 10) day = `0${day}`;
    if(month < 10) month = `0${month}`;
    if(hours < 10) hours = `0${hours}`;
    if(minutes < 10) hours = `0${minutes}`;
    if(seconds < 10) hours = `0${seconds}`; 
    var formated_date = `${day}.${month}.${year} ${hours}:${minutes}:${seconds}`;
    return formated_date;
}

