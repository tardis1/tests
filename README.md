# Tests

Наборы тестов для Тардиса и TardisAgent

### Клонирование репозитория
Чтобы скопировать репозиторий необходимо создать аккаунт GitLab, выполнить команду копирования:
``` 
git clone -b dev --single-branch https://gitlab.com/tardis1/tests.git
```

Затем открыть проект в VS Code, переключиться на ветку dev, внести изменения, выполнить команду `git commit`, затем `git push`. При первом push в VS Code нужно будет ввести логин пароль GitLab.

Далее перед внесением изменений выполнять команду `git pull` для обновления репозитория.

### Запуск тестов
 
 - start.bat - запускает базу test manager и открывает bddRunner. 

 - auto.bat - авто запуск сценариев, сохранение и создание отчета.

 - report.bat - создает отчет cucumber.

### Полезные ресурсы

 - Создание сценариев
   https://v8book.ru/public/992320/
 - Автоматизация запуска сценариев
   http://katalog.elrey-uslugi.ru/public/1010127/#%D0%92%D0%B0%D1%80%D0%B8%D0%B0%D0%BD%D1%82%D1%8B_%D0%B0%D0%B2%D1%82%D0%BE%D0%BC%D0%B0%D1%82%D0%B8%D1%87%D0%B5%D1%81%D0%BA%D0%BE%D0%B3%D0%BE_%D0%B7%D0%B0%D0%BF%D1%83%D1%81%D0%BA%D0%B0
 - Примеры файлов настроек
   https://github.com/vanessa-opensource/add/tree/master/tools/JSON

### Разные настройки

// Запуск через bat файл
"C:\Program Files (x86)\1cv8\8.3.18.1208\bin\1cv8.exe" ENTERPRISE /TESTMANAGER /F"\\cloud-prod-fs-1\base1c-2\company_8503\fileinfobases\1c_my_8503_23" /Execute "D:\OneScript\lib\add\bddRunner.epf" /N"Docarchive" /P"" /C"StartFeaturePlayer;workspaceRoot=D:\dev\Vanessa_tests\tardis_agent\features;VBParams=D:\dev\Vanessa_tests\tardis_agent\settings\bddRunnerAutostartSettings.json"

// Запуск через 1Script
runner vanessa --settings "D:\dev\Vanessa_tests\tardis_agent\settings\VanessaRrunnerSettings.json"
runner vanessa --pathvanessa "D:\OneScript\lib\add\bddRunner.epf" --vanessasettings "D:\dev\Vanessa_tests\features\tardis_agent\bddRunnerSettings.json" --ibconnection /Fcloud-prod-fs-1\base1c-2\company_8503\fileinfobases\1c_my_8503_23 --db-user "Docarchive" --db-pwd ""

// Настройки подключения к базам
Файловая 7арт
File="\\cloud-prod-fs-1\base1c-2\company_8503\fileinfobases\1c_my_8503_23";
/N"Docarchive"/P""
/F"\\cloud-prod-fs-1\base1c-2\company_8503\fileinfobases\1c_my_8503_23" /N"Docarchive" /P""

// Серверная 7арт
Srvr="beta-tardis";Ref="1c_my_2162_1";
/NVanessaTester /DisableStartupMessages /AllowExecuteScheduledJobs -Off
/S"beta-tardis;Ref="1c_my_2162_1" /N"Docarchive" /P""


### Отчет Cucumber
 - https://www.npmjs.com/package/cucumber-html-reporter
 ```
 npm install cucumber-html-reporter
 ```
 //Файл отчета из Ванессы сохранять в 
 "D:\dev\Vanessa_tests\tardis\reports\cucumber\CucumberJson.json"
 "D:\dev\Vanessa_tests\tardis_agent\reports\cucumber\CucumberJson.json"
 
 //Отчет (cucumber_report.html) формируется и сразу открывается командой
 ```
 node index.js
 ```
 в каталогах
 D:\dev\Vanessa_tests\tardis\reports\cucumber>  node index.js
 D:\dev\Vanessa_tests\tardis_agent\reports\cucumber> node index.js
