#language: ru

@IgnoreOnCIMainBuild
@ExportScenarios

Функционал: Задачи пользователей

Сценарий: Задачи пользователей "п_пользователь" "п_организация"
    И Я открываю навигационную ссылку "e1cib/list/Документ.ЗадачиПользователей"
    И я нажимаю на кнопку 'Исходящие'
    # запись задачи
    И в таблице "СписокЗадач" я нажимаю на кнопку 'Создать'
    И открылась форма с именем "Документ.ЗадачиПользователей.Форма.ФормаДокумента"
    И я нажимаю на кнопку 'Записать в черновик'
    И я жду, что таблица "СписокЗадач" станет равна данной в течение 5 секунд:
    | Статус      |
	| 'Черновик'  |
    И в таблице "СписокЗадач" количество строк "равно" 1
	И в таблице "СписокЗадач" я перехожу к первой строке 
	И в таблице "СписокЗадач" я выбираю текущую строку
    И открылась форма с именем "Документ.ЗадачиПользователей.Форма.ФормаДокумента"
    # проведение задачи
    И из выпадающего списка с именем "Исполнитель" я выбираю по строке "п_пользователь"
    И из выпадающего списка с именем "Организация" я выбираю по строке "п_организация"
    И в поле 'Срок выполнения' я ввожу конец текущего месяца
    И в поле 'Текст' я ввожу текст 'текст'
    И я нажимаю на кнопку 'Отправить исполнителю'
    И открылась форма с именем "Документ.ЗадачиПользователей.Форма.ФормаСписка"
    И я жду, что таблица "СписокЗадач" станет равна данной в течение 5 секунд:
    |  Организация      | Исполнитель         | Статус  |
	| "п_организация"   | "п_пользователь"    | "Новая" |
    # комментарии
    И в таблице "СписокЗадач" я нажимаю на кнопку 'Комментарий'
    Тогда открылось окно 'Введите комментарий'
	И в поле 'InputFld' я ввожу текст 'комментарий'
	И я нажимаю на кнопку 'OK'
    И я нажимаю на кнопку 'Мои задачи'
     И в таблице "СписокЗадач" я нажимаю на кнопку 'Комментарий'
	Тогда открылось окно 'Введите комментарий'
	И в поле 'InputFld' я ввожу текст 'новый комментарий'
	И я нажимаю на кнопку 'OK'
     # статусы задачи
    И в таблице "СписокЗадач" я нажимаю на кнопку 'Принять в работу'
    И я жду, что таблица "СписокЗадач" станет равна данной в течение 5 секунд:
    |  Организация      | Автор               | Статус             |
	| "п_организация"   | "п_пользователь"    | "Принята в работу" | 
    И в таблице "СписокЗадач" я нажимаю на кнопку 'Отправить на проверку'
	Тогда открылось окно 'Трудозатраты по задаче (часы)'
	И в поле 'InputFld' я ввожу текст '5'
	И я нажимаю на кнопку 'OK'
	И в таблице "СписокЗадач" я нажимаю на кнопку 'Команда обновить интерфейс'
    И я жду, что таблица "СписокЗадач" станет равна данной в течение 5 секунд:
    |  Организация      | Автор               | Статус                             | Трудозатраты, ч |
	| "п_организация"   | "п_пользователь"    | "Ожидает подтверждения выполнения" | 5				 | 
	И я нажимаю на кнопку 'Исходящие'
    И в таблице "СписокЗадач" я нажимаю на кнопку 'Вернуть на доработку'
    И я жду, что таблица "СписокЗадач" станет равна данной в течение 5 секунд:
    |  Организация      | Исполнитель         | Статус                    | Трудозатраты, ч |
	| "п_организация"   | "п_пользователь"    | "Возвращена на доработку" | 5				|  
	И я нажимаю на кнопку 'Мои задачи'
    И в таблице "СписокЗадач" я нажимаю на кнопку 'Принять в работу'
	И в таблице "СписокЗадач" я нажимаю на кнопку 'Отправить на проверку'
	И     Пауза 1
	Тогда открылось окно 'Трудозатраты по задаче (часы)'
	И     Пауза 1
	И в поле 'InputFld' я ввожу текст '7'
	И     Пауза 1
	И я нажимаю на кнопку 'OK'
	И     Пауза 1
	И в таблице "СписокЗадач" я нажимаю на кнопку 'Команда обновить интерфейс'
    И я жду, что таблица "СписокЗадач" станет равна данной в течение 5 секунд:
    |  Организация      | Автор               | Статус                             | Трудозатраты, ч |
	| "п_организация"   | "п_пользователь"    | "Ожидает подтверждения выполнения" | 12				 |
	И я нажимаю на кнопку 'Исходящие'
    И в таблице "СписокЗадач" я нажимаю на кнопку 'Принять '
    И я жду, что таблица "СписокЗадач" станет равна данной в течение 5 секунд:
    |  Организация      | Исполнитель         | Статус      | Трудозатраты, ч |
	| "п_организация"   | "п_пользователь"    | "Выполнена" | 12			  | 
	# история изменений
    И в таблице "СписокЗадач" я нажимаю на кнопку 'История изменений'
	И открылась форма с именем "Документ.ЗадачиПользователей.Форма.ИсторияИзменений"
	И в таблице "ИсторияИзменений" я разворачиваю текущую строку
    И я жду, что таблица "ИсторияИзменений" станет равна данной в течение 5 секунд:
    | Статус                                |
	| ""                                    |
    | "Новая"                               |
    | "Принята в работу"                    |
    | "Ожидает подтверждения выполнения"    |
    | "Возвращена на доработку"             |
    | "Принята в работу"                    |
    | "Ожидает подтверждения выполнения"    |
    | "Выполнена"                           |     
    И Я закрываю окно 'История изменений'
    # удаление
    И я выбираю пункт контекстного меню "Удалить задачу" на элементе формы с именем "СписокЗадач"
	Тогда открылось окно '1С:Предприятие'
	И я нажимаю на кнопку 'Да'
	И Я закрываю текущее окно
	И Я закрыл все окна клиентского приложения
