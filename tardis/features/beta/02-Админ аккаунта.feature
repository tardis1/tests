#language: ru

Функционал: Проверка Tardis от пользователя админ аккаунта


Сценарий: Подключение TestClient
	Когда Я подключаю клиент тестирования с параметрами:
    | 'Имя подключения'| 'Логин'             | 'Пароль' 		| 'Дополнительные параметры строки запуска'                    |
    | 't_beta'         | 'vanessa_admin'     | 'Vanessa1'       | '/DisableStartupMessages /AllowExecuteScheduledJobs -Off'    |

# АРМ бухгалтера

Структура сценария: Вкладка общая
	И Я открываю навигационную ссылку "e1cib/app/Обработка.АРМбухгалтера"
	И Открытие вкладки Общая
	# И я изменяю флаг 'Включать контроль ЖХО'
	# И я изменяю флаг 'Отобразить все базы'
	И из выпадающего списка "Пользователь АРМ" я выбираю точное значение <Пользователь>
Примеры:
	| Аккаунт      	 | Подразделение  | Пользователь	| Организация  |
	| "test_account" | "test_subunit" | "vanessa_admin" | "Экосорбер"  |


Структура сценария: Вкладка автрибутирование
	И Открытие вкладки Атрибутирование <Пользователь> <Организация>
Примеры:
	| Аккаунт      	 | Подразделение  | Пользователь	| Организация  |
	| "test_account" | "test_subunit" | "vanessa_admin" | "Экосорбер"  |


Сценарий: Вкладка аудит организаций
	И Открытие вкладки Аудит организаций


Структура сценария: Вкладка эффективность работы бухгалтера
	И Открытие вкладки Эффективность работы бухгалтера <Подразделение>
Примеры:
	| Аккаунт      	 | Подразделение  | Пользователь	| Организация  |
	| "test_account" | "test_subunit" | "vanessa_admin" | "Экосорбер"  |


Структура сценария: Вкладка контроль атрибутирования
	И Открытие вкладки Контроль атрибутирования <Пользователь> <Подразделение>
Примеры:
	| Аккаунт      	 | Подразделение  | Пользователь	| Организация  |
	| "test_account" | "test_subunit" | "vanessa_admin" | "Экосорбер"  |


Структура сценария: Вкладка нулевая отчетность
	И Открытие вкладки Нулевая отчетность <Пользователь> <Организация>		
Примеры:
	| Аккаунт      	 | Подразделение  | Пользователь	| Организация  |
	| "test_account" | "test_subunit" | "vanessa_admin" | "Экосорбер"  |

# Отчеты

Структура сценария: Отчет анализ выполнения сервисных операций
	И Отчет Анализ выполнения сервисных операций <База> <Операция>
Примеры:
| Аккаунт      	 | Подразделение  | Пользователь	| База 		   			      | Организация | Операция 				|    
| "test_account" | "test_subunit" | "vanessa_admin" | "bp_base1_1265 [42_8503_9]" | "Экосорбер" | "Контроль отчетности"	|


Структура сценария: Отчет анализ показателей APDEX
	И Отчет Анализ показателей APDEX <База> "BuhAddObject/Item"
Примеры:
| Аккаунт      	 | Подразделение  | Пользователь	| База 		   			      | Организация | Операция 				|    
| "test_account" | "test_subunit" | "vanessa_admin" | "bp_base1_1265 [42_8503_9]" | "Экосорбер" | "Контроль отчетности"	|


Структура сценария: Отчет взаимодействие с ИФНС
	И Отчет Взаимодействие с ИФНС <Организация> "Требование"
Примеры:
| Аккаунт      	 | Подразделение  | Пользователь	| База 		   			      | Организация | Операция 				|    
| "test_account" | "test_subunit" | "vanessa_admin" | "bp_base1_1265 [42_8503_9]" | "Экосорбер" | "Контроль отчетности"	|


Структура сценария: Отчет задолженность организаций
	И Отчет Задолженность организаций <Пользователь> <Подразделение>
Примеры:
| Аккаунт      	 | Подразделение  | Пользователь	| База 		   			      | Организация | Операция 				|    
| "test_account" | "test_subunit" | "vanessa_admin" | "bp_base1_1265 [42_8503_9]" | "Экосорбер" | "Контроль отчетности"	|


Структура сценария: Отчет контроль атрибутирования
	И Отчет Контроль атрибутирования <Пользователь> <Подразделение>
Примеры:
| Аккаунт      	 | Подразделение  | Пользователь	| База 		   			      | Организация | Операция 				|    
| "test_account" | "test_subunit" | "vanessa_admin" | "bp_base1_1265 [42_8503_9]" | "Экосорбер" | "Контроль отчетности"	|


Структура сценария: Отчет налоговая нагрузка
	И Отчет Налоговая нагрузка <Подразделение> <Организация>
Примеры:
| Аккаунт      	 | Подразделение  | Пользователь	| База 		   			      | Организация | Операция 				|    
| "test_account" | "test_subunit" | "vanessa_admin" | "bp_base1_1265 [42_8503_9]" | "Экосорбер" | "Контроль отчетности"	|


Структура сценария: Отчет оценка аудиторов
	И Отчет Оценка аудиторов <Пользователь> <Подразделение> <Организация>
Примеры:
| Аккаунт      	 | Подразделение  | Пользователь	| База 		   			      | Организация | Операция 				|    
| "test_account" | "test_subunit" | "vanessa_admin" | "bp_base1_1265 [42_8503_9]" | "Экосорбер" | "Контроль отчетности"	|


Структура сценария: Отчет результаты внутреннего аудита
	И Отчет Результаты внутреннего аудита <Пользователь> <Подразделение> <Организация> <Аккаунт>
Примеры:
| Аккаунт      	 | Подразделение  | Пользователь	| База 		   			      | Организация | Операция 				|    
| "test_account" | "test_subunit" | "vanessa_admin" | "bp_base1_1265 [42_8503_9]" | "Экосорбер" | "Контроль отчетности"	|


Структура сценария: Отчет результаты кросс-аудита
	И Отчет Результаты кросс-аудита <Пользователь> <Организация>
Примеры:
| Аккаунт      	 | Подразделение  | Пользователь	| База 		   			      | Организация | Операция 				|    
| "test_account" | "test_subunit" | "vanessa_admin" | "bp_base1_1265 [42_8503_9]" | "Экосорбер" | "Контроль отчетности"	|


Структура сценария: Отчет статистика работы бухгалтера
	И Отчет Статистика работы бухгалтера <Пользователь> <Подразделение> <Организация>
Примеры:
| Аккаунт      	 | Подразделение  | Пользователь	| База 		   			      | Организация | Операция 				|    
| "test_account" | "test_subunit" | "vanessa_admin" | "bp_base1_1265 [42_8503_9]" | "Экосорбер" | "Контроль отчетности"	|


Структура сценария: Отчет статистика работы пользователей
	И Отчет Статистика работы пользователей <Пользователь> <Аккаунт>
Примеры:
| Аккаунт      	 | Подразделение  | Пользователь	| База 		   			      | Организация | Операция 				|    
| "test_account" | "test_subunit" | "vanessa_admin" | "bp_base1_1265 [42_8503_9]" | "Экосорбер" | "Контроль отчетности"	|


Структура сценария: Отчет эффективность работы бухгалтера
	И Отчет Эффективность работы бухгалтера <Подразделение>
Примеры:
| Аккаунт      	 | Подразделение  | Пользователь	| База 		   			      | Организация | Операция 				|    
| "test_account" | "test_subunit" | "vanessa_admin" | "bp_base1_1265 [42_8503_9]" | "Экосорбер" | "Контроль отчетности"	|

# Разное

# Сценарий: Авторизация
# 	И Авторизация пользователя


Структура сценария: Базы
	И Карточка базы <ИдБазы>
Примеры:
| База							| ИдБазы     								|
| "bp_base1_1265 [42_8503_9]"	| "a7f2d5bd-9e75-4fde-acba-e2330607b42d"	|


Структура сценария: Организации
	И Карточка организации <ИдКаталога> <Хранилище>
Примеры:
| Организация | ИдКаталога     						| Хранилище 		 |
| "Экосорбер" | "1Ujvq7DkgkwW3tdeADDw1QkoutZ4bEgeA"	| "TD MCOB D3 beta"  |


Структура сценария: Подключение сбис
	И Подключение сбис <Организация>
Примеры:
| Организация |
| "ЭДО сбис"  |


Структура сценария: Подключение контур
	И Подключение контур <Организация> <ИдКонтур>
Примеры:
| Организация  | ИдКонтур     							|
| "ЭДО контур" | "7b7dba2f-a7b5-457e-a135-e340c26536d9"	|


Структура сценария: Пользователи
	И Карточка пользователей <Логин>
Примеры:
| Логин  			| 
| "vanessa_admin"   |


Структура сценария: Помощник заполнение реквизитов
	И Помощник заполнение реквизитов <Пользователь> <Подразделение> <База>  <Конфигурация>
Примеры:
	| Пользователь    | Подразделение	 | База 		   				| Конфигурация 									|
	| "vanessa_admin" | "test_subunit" 	 | "bp_base1_1265 [42_8503_9]"  | "БП Проф. ред. 3.0 + Ext. TardisAgent api_v2" |


Структура сценария: Помощник ввод начальных данных
	И Помощник ввод начальных данных <БазаРасшифровка>
Примеры:
	| БазаРасшифровка    			|
	| "bp_base1_1265 [42_8503_9]" 	|


Структура сценария: Задачи пользователей
	И Задачи пользователей <Пользователь> <Организация>
Примеры:
| Пользователь	  | Организация |
| "vanessa_admin" | "Экосорбер" |

Сценарий: Отключение TestClient
	Когда я закрываю сеанс TESTCLIENT		
